#!/usr/bin/bash

pwd=$(pwd)

mkdir /tmp/syncing-gits
cp gits.txt /tmp/syncing-gits/
cd /tmp/syncing-gits


for gits in $(grep "^[^#]" gits.txt | gits.txt)
do
    url1=$(echo $gits | cut -f 1 -d';')
    b1=$(echo $gits | cut -f 2 -d';')
    url2=$(echo $gits | cut -f 3 -d';')
    b2=$(echo $gits | cut -f 4 -d';')

    git clone -o git1 ${url1} clone
    cd clone
    git remote add git2 ${url2}
    git pull git2 ${b2}



    git1=$(git cherry git1/${b1} git2/${b2})
    git2=$(git cherry git2/${b2} git1/${b1})

    if [ "x${git1}" = "x" ]; then
        if [ "x${git2}" != "x" ]; then 
            echo "${url1} is newer"
            git branch git2-temporary --track git2/${b2}
            git checkout git2-temporary
            git merge git1/${b1}
            git push git2 HEAD:${b2}
        fi
    else
        if [ "x${git2}" = "x" ]; then
            echo "${url2} is newer"
            git branch git1-temporary --track git1/${b1}
            git checkout git1-temporary
            git merge git2/${b2}
            git push git1 HEAD:${b1}
        else
            echo "There is a conflict between pagure and gitlab"
        fi
    fi

    cd $pwd
    rm -rf /tmp/syncing-gits/*
done

cd $pwd
rmdir /tmp/syncing-gits

exit 0
