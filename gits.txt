#This file contains list of git repos and branches which need to be synchronized
#If the line starts with # it is ignored.
#Each line contains two repos with branches to be synchronized.
#The format is: <url to git repo 1>;<branch in repo 1>;<url to git repo 2>;<branch in repo 2>
#git1;branch1;git2;branch2
